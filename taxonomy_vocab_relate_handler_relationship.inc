<?php

/**
 * Create the two joins for the relationship
 */
class taxonomy_vocab_relate_handler_relationship extends views_handler_relationship {
  /**
   * Implement the relationship
   */
  function query() {
    // Figure out what base table this relationship brings to the party.
    $table_data = views_fetch_data($this->definition['base']);
    $base_field = empty($this->definition['base field']) ? $table_data['table']['base']['field'] : $this->definition['base field'];

    $this->ensure_my_table();

    $def = $this->definition;
    $def['table'] = $this->definition['base'];
    $def['field'] = $base_field;
    $def['left_table'] = $this->table_alias;
    $def['left_field'] = $this->field;
    if (!empty($this->options['required'])) {
      $def['type'] = 'INNER';
    }
    
    $join = new views_join();
    $join->definition = array(
      'table' => 'term_node',
      'field' => 'nid',
      'left_table' => 'node',
      'left_field' => 'nid',
    );
    
    $join->construct();
    
    // use a short alias for this:
    $alias = $join->definition['table'] . '_term_node';
    $this->alias = $this->query->add_relationship($alias, $join, 'term_node', $this->relationship);
    
    $join = new views_join();
    $join->definition = array(
      'table' => 'term_data',
      'field' => 'tid',
      'left_table' => 'term_node',
      'left_field' => 'tid',
    );
    
    $join->construct();
    
    
    // use a short alias for this:
    $alias = $join->definition['table'] . '_term_data';
    $this->alias = $this->query->add_relationship($alias, $join, 'term_data', $this->relationship);
    
    $join = new views_join();
    $join->definition = array(
      'table' => 'term_relation',
      'field' => 'tid1',
      'left_table' => 'term_data',
      'left_field' => 'tid',
    );
    
    $join->construct();
    
    // use a short alias for this:
    $alias = $join->definition['table'] . '_term_relation';
    $this->alias = $this->query->add_relationship($alias, $join, 'term_relation', $this->relationship);
    
    $join = new views_join();
    $join->definition = array(
      'table' => 'term_data',
      'field' => 'tid',
      'left_table' => 'term_relation',
      'left_field' => 'tid2',
    );
    
    $join->construct();
    
    // use a short alias for this:
    $alias = $join->definition['table'] . '_term_data2';
    $this->alias = $this->query->add_relationship($alias, $join, 'term_data', $this->relationship);
  }
}

