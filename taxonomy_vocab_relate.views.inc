<?php

/**
 * Implementation of hook_views_data()
 */
function taxonomy_vocab_relate_views_data_alter(&$data) {
  $data['node']['tvr_rel'] = array(
    'group' => t('Taxonomy'),
    'title' => t('Taxonomy Vocabulary Relate'),
    'help' => t('The relation term of the term. This can produce duplicate entries if you are using a vocabulary that allows multiple parents.'),
    'relationship' => array(
      'handler' => 'taxonomy_vocab_relate_handler_relationship',
      'base' => 'term_node',
      'base field' => 'nid',
      'relationship field' => 'nid',
      'field' => 'nid',
      'label' => t('TVR'),
    ),
  );
  return $data;
}

/**
 * Define the views handlers
 */
function taxonomy_vocab_relate_views_handlers() {
  return array(
    'handlers' => array(
      'taxonomy_vocab_relate_handler_relationship' => array(
        'parent' => 'views_handler_relationship',
      ),
    ),
  );
}